# Core: essential functions and parameters

readonly _UTIL_CORE=1

function echoe() {
	echo $@ >&2
}

function die() {
	local -i ecode=$?
	local str="$@"
	[[ 0 -eq $ecode ]] \
		|| str+=" (error: ${ecode})"

	if declare -F log-err &>/dev/null; then
		log-crit ${str}
	else
		echoe "ERROR: ${str}"
	fi
	exit 1
}

# vim:ft=bash
