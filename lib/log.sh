# Log: logging functions

readonly _UTIL_LOG=1

[[ -v _UTIL_CORE ]] || {
	echo 'core script must be sourced first' >&2
	exit 1
}

command -v logger &>/dev/null \
	|| die '"logger" is a required dependency'

function log-msg() {
	local -r priority=${1:?logging priority is undefined}
	local -r msg="${@:2}"

	local -ri pid=$$

	if [[ -v DEBUG ]]; then
		echoe "<${priority^^}> [${pid}]: ${msg}"
	else
		logger \
			${DEBUG_LOG:+--stderr} \
			--priority "daemon.${priority}" \
			--id=${pid} \
			${msg}
	fi
}

function log-debug()    { log-msg debug     $@; }
function log-info()     { log-msg info      $@; }
function log-notice()   { log-msg notice    $@; }
function log-warn()     { log-msg warning   $@; }
function log-err()      { log-msg err       $@; }
function log-crit()     { log-msg crit      $@; }

# vim:ft=bash
