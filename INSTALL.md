# Installing

## Prerequisites

Global dependencies:

- `bash`

To use the this collection, the following ArchLinux package dependencies are
needed for each script, respectively:

- __`core.sh`:__ (none)
- __`log.sh`:__ `util-linux`
