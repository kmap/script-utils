# KMap Script Utilities

A set of `bash` scripts meant to be sourced to provide common functions and
parameters for scripts in the KMap project.
